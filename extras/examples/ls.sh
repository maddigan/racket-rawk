#!/bin/sh


# shellcheck disable=2012,2016


ls -ahl \
    | racket -l 'rawk/tool/rawk-exe' -- \
             -F ' +' \
             '.* { (print ($s 0) " " ($s 8)) }'
