#!/bin/sh


# "racket -l 'rawk/tool/rawk-exe'" imports "rawk-exe"
# skipping the requirement of "rawk" being available on "PATH".

# shellcheck disable=2012,2016


echo 'Input some text, maybe "Hello, how are You?" for example ;)'
echo 'Press Ctrl+D to exit.'

racket -l 'rawk/tool/rawk-exe' -- \
       -F ',' \
       '.* { (print "+ Line: " line "\n" "+ 1st field: " ($s 0)) }'
