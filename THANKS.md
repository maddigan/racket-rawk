# Thank You to


+ Jens Axel Søgaard (soegaard)
  for:
  - help with syntax-parse,

+ Matthew Flatt (mflatt)
  for:
  - help with namespaces,
    commit: `4447f9b39ef7a839966d865d024765da442c7f7a`.
