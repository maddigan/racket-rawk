MAKE            ?= make
RACKET          := racket
RACO            := raco
SCRIBBLE        := $(RACO) scribble

PWD             ?= $(shell pwd)
BIN             := $(PWD)/bin
EXTRAS          := $(PWD)/extras
COMPLETION      := $(EXTRAS)/completion
DOCS            := $(PWD)/docs
PUBLIC          := $(PWD)/public
SCRIPTS         := $(PWD)/scripts
SRC             := $(PWD)/src


.PHONY: all
all: compile

src-%:
	$(MAKE) -C src $(*)

.PHONY: clean
clean: src-clean
	if [ -d $(BIN) ] ; then rm -r $(BIN) ; fi
	if [ -d $(PUBLIC) ] ; then rm -r $(PUBLIC) ; fi

.PHONY: compile
compile: src-compile

.PHONY: install
install: src-install

.PHONY: setup
setup: src-setup

.PHONY: shellcheck
shellcheck:
	find $(PWD) -type f -name "*.sh" -exec shellcheck {} +

.PHONY: test
test: src-test

.PHONY: remove
remove: src-remove

$(BIN):
	mkdir -p $(BIN)
	$(RACO) exe --orig-exe -v -o $(BIN)/rawk \
		$(SRC)/rawk-tool/rawk/tool/rawk-exe.rkt

.PHONY: bin
bin:
	$(MAKE) -B $(BIN)

$(COMPLETION)/zsh/_rawk:
	ziptie-make-completion -t zsh rawk > extras/completion/zsh/_rawk

.PHONY: completion
completion:
	$(MAKE) -B $(COMPLETION)/zsh/_rawk

$(PUBLIC):
	$(SCRIBBLE) ++main-xref-in --dest $(PWD) --dest-name public \
		--htmls --quiet $(DOCS)/scribblings/main.scrbl

.PHONY: public
public:
	$(MAKE) -B $(PUBLIC)
